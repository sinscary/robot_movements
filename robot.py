class Robot:

    def __init__(self):
        """
        Initialize the grid length and width of 10
        """
        self.width = 10
        self.height = 10
        self.grid = []
    def createMatrix(self):
        """
        Function to create the matrix, i.e, Initialize all matrix value as zero
        and calling readMatrix function to read the initial position of robot
        """
        self.grid = [[0 for i in range(self.height)] \
                for j in range(self.width)]

    def readCommand(self):
        """
        In this function read the user input
        """
        print("Enter initial location of the robot")
        position = input().strip("()").split(",")
        print("Enter initial direction")
        initial_direction = input()
        print("Enter command to move robot")
        command = input().split(",")
        pos_x = int(position[0])
        pos_y = int(position[1])
        if pos_x >9 or pos_y > 9:
            print("Invalid position")
            return
        if initial_direction=='F' :
            #Calling method that reads command entered by user when robot direction is 'F'
            self.moveRobotF(pos_x, pos_y, initial_direction, command)

        elif initial_direction == 'L':
            self.moveRobotL(pos_x,pos_y, initial_direction, command)

        elif initial_direction == 'R':
            self.moveRobotR(pos_x,pos_y, initial_direction, command)

    def moveRobotF(self, pos_x, pos_y, initial_direction, command):
        """
        Function to Move robot when initial_direction is F
        """
        length = len(command)
        for i in range(0, length):
            """
            Here check the command and based on that update the robot position
            """
            if command[i] == 'F' and pos_y<9:
                pos_y+=1
            elif command[i] == 'R' and pos_x<9 and pos_x > 0:
                pos_x+=1
            elif command[i] == 'L' and pos_x<9 and pos_x > 0:
                pos_x-=1
            else:
                print ("Position not reachable")
                return
        self.grid[pos_x][pos_y] = 1 #update grid with new positions of robot
        print ("New Position is: %s %s" %(pos_x, pos_y))

    def moveRobotR(self, pos_x, pos_y, initial_direction, command):
        """
        Function to Move robot when initial_direction is R
        """
        length = len(command)
        for i in range(0, length):
            """
            Here check the command name and based on that update the robot position
            """
            if command[i] == 'F' and pos_x<9 and pos_x > 0:
                pos_x+=1
            elif command[i] == 'R' and pos_y<9 and pos_y > 0:
                pos_y-=1
            elif command[i] == 'L' and pos_y<9 and pos_y > 0:
                pos_y+=1
            else:
                print ("Position not reachable")
                return
        self.grid[pos_x][pos_y] = 1 #update grid with new positions of robot
        print ("New Position is: %s %s" %(pos_x, pos_y))

    def moveRobotL(self, pos_x, pos_y, initial_direction, command):
        """
        Function to Move robot when initial_direction is L
        """
        length = len(command)
        for i in range(0, length):
            """
            Here check the command name and based on that update the robot position
            """
            if command[i] == 'F' and pos_x<9 and pos_x > 0:
                pos_x-=1
            elif command[i] == 'R' and pos_y<9 and pos_y > 0:
                pos_y+=1
            elif command[i] == 'L' and pos_y<9 and pos_y > 0:
                pos_y-=1
            else:
                print ("Position not reachable")
                return
        self.grid[pos_x][pos_y] = 1 #update grid with new positions of robot
        print ("New Position is: %s %s" %(pos_x, pos_y))

rob = Robot()
rob.createMatrix()
rob.readCommand()
